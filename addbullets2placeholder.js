// Successfully runs and updates with bulletpoints to the BODY of a TITLE_AND_BODY template
// As of 17FEB2023

const { google } = require('googleapis');
const {GoogleAuth} = require('google-auth-library');

async function addBulletsToBodyPlaceholder(slideId, objectId, textArray) {
  const auth = new GoogleAuth({
    scopes:['https://www.googleapis.com/auth/presentations',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/presentations.readonly'],
    keyFilename: 'client_key.json'
  });

  const slides = google.slides({ version: 'v1', auth });
  const presentationId = '1Dm130WneNI0B6iHon5i4sgta_BWth10bNl2K9t7nZo8';

  try {
    const { data: { replies } } = await slides.presentations.batchUpdate({
      presentationId,
      requestBody: {
        requests: [
          {
            insertText: {
              objectId,
              text: textArray.join('\n')
            }
          },
          {
            createParagraphBullets: {
              objectId,
              bulletPreset: 'BULLET_DISC_CIRCLE_SQUARE',
              textRange: {
                type: 'ALL'
              }
            }
          }
        ]
      }
    });

    console.log(`Updated ${replies.length} page elements:`);
    replies.forEach((reply) => console.log(reply));
  } catch (err) {
    console.error(`Error adding bullets to body placeholder on slide ${slideId}: ${err}`);
    throw err;
  }
}

// Example usage
const textArray = ['Task 1: Get Service Account Access Key', 'Task 2: Generate JWT Token', 'Task 3: Use token in API calls'];
addBulletsToBodyPlaceholder('Header3', 'Header3-body', textArray)
  .catch((err) => console.error(err));
