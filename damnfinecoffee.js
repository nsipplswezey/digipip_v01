const { google } = require('googleapis');
const {GoogleAuth} = require('google-auth-library');
const fs = require('fs');

// Set up the service account credentials
async function main(markdownpath) {
  
  const folderId = '1BplNrIuoL374eW_HhbXtgxnvkSrDY8dW';
  
  let presentationId;
  let titlePres;
  let slidetitlearray;
  let slidebulletarray;

  // SA account key
  const auth = new GoogleAuth({
    scopes:['https://www.googleapis.com/auth/presentations',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/presentations.readonly'],
    keyFilename: 'recoll.json'
  });

  // Set up the Google Drive and Slides APIs clients
  const drive = google.drive({ version: 'v3' , auth });
  const slides = google.slides({ version: 'v1', auth });

  try {
    const data = await fs.promises.readFile(markdownpath, 'utf8');
    
    const regexTitle = /^# (.*)$/m; // regex to match everything following a single "# " 
    redata = data.match(regexTitle);
    titlePres = redata[1];

    //regex to match everything following "## "
    const regexSlideTitle = /## (.*)/g;
    const matches = [...data.matchAll(regexSlideTitle)];
    slidetitlearray = matches.map(match => {
      return match[1];
    })

    //regex to match everything following "- "
    slidebulletarray = [];
    let inBullet = false;
    let bullet = [];

    data.split("\n").forEach((line) => {
      if (line.startsWith("- ")) {
        if (!inBullet) {
          inBullet = true;
          bullet = [line];
        } else {
          bullet.push(line);
        }
      } else if (inBullet) {
        slidebulletarray.push(bullet.join("\n"));
        inBullet = false;
      }
    });

    if (inBullet) {
      slidebulletarray.push(bullet.join("\n"));
    }
  } catch (err) {
    console.log("Problem with regexing")       
    console.error(err)
  };

  
  // Create a new presentation
  slides.presentations.create({
    requestBody: {
      title: titlePres
    }
  }).then(res => {
    presentationId = res.data.presentationId;
    
    // Create slides with the titles
    const requests = slidetitlearray.map(data => {
      //console.log(data)
      return {
        createSlide: {
          objectId: data.replace(/\s/g, ''),
          slideLayoutReference: {
            predefinedLayout: 'TITLE_AND_BODY',
          },
          insertionIndex: 1 + slidetitlearray.indexOf(data),
          placeholderIdMappings: [
            {
              layoutPlaceholder: {
                type: 'TITLE'
              },
              objectId: data.replace(/\s/g, '') + '-title'
            },
            {
              layoutPlaceholder: {
                type: 'BODY'
              },
              objectId: data.replace(/\s/g, '') + '-body'
            }
          ]
        }
      };
    });
    slides.presentations.batchUpdate({
      presentationId,
      requestBody: {
        requests
      }
    }).then(res => {
      console.log(`Created ${slidetitlearray.length} slides with titles.`);
      drive.files.update({
        fileId: presentationId,
        addParents: folderId
      }).then(res => {
          console.log(`Presentation added to folder with ID: ${folderId} and presentation ID: ${presentationId}`);
          addSlideTitles(presentationId, slidetitlearray)
          addSlideBullets(presentationId, slidetitlearray, slidebulletarray)
        }).catch(err => {
          console.error(`Error adding presentation to folder: ${err}`);
        });
      }).catch(err => {
        console.error(`Error creating slides: ${err}`);
      });
    }).catch(err => {
    console.error(`Error creating presentation: ${err}`);
  });
}


async function addSlideTitles(presentation, titleArray) {
    const { google } = require('googleapis');
    const {GoogleAuth} = require('google-auth-library');
    

    const auth = new GoogleAuth({
      scopes:['https://www.googleapis.com/auth/presentations',
      'https://www.googleapis.com/auth/drive.file',
      'https://www.googleapis.com/auth/drive',
      'https://www.googleapis.com/auth/drive.readonly',
      'https://www.googleapis.com/auth/presentations.readonly'],
      keyFilename: 'recoll.json'
    });
  
    const slides = google.slides({ version: 'v1', auth });
    
    // ID of the presentation you want to modify
const presentationId = presentation;

// An array of text strings to use as page titles
const titles = titleArray;

// Generate the updatePageProperties requests using map
const requests = titles.map(title => ({
  insertText: {
    objectId: title.replace(/\s/g, '') + '-title',
    text: title
  },
}));

// Concatenate the requests array into a single array
const batchUpdateRequest = {
  requests: [...requests],
};

// Make the batchUpdate request to the Slides API
slides.presentations.batchUpdate(
  { presentationId, requestBody: batchUpdateRequest },
  (err, res) => {
    if (err) return console.error('The API returned an error:', err);
    console.log(`Page titles updated for presentation with ID: ${presentationId}`);
  });
}

async function addSlideBullets(presentation, titleArray, bulletArray) {
  const { google } = require('googleapis');
  const {GoogleAuth} = require('google-auth-library');
  

  const auth = new GoogleAuth({
    scopes:['https://www.googleapis.com/auth/presentations',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/presentations.readonly'],
    keyFilename: 'recoll.json'
  });

  const slides = google.slides({ version: 'v1', auth });
  
  // ID of the presentation you want to modify
  const presentationId = presentation;


  // console.log(bulletArray)
  // An array of text strings to use as page titles
  const titles = titleArray;
  const bulletPoints = bulletArray;

  
  const slidesbullets = titles.map((title, index) => ({
    title: title.replace(/\s/g, '') + '-body',
    bullets: bulletPoints[index]
  }));

  // console.log(slidesbullets)

  const requests = slidesbullets.map(slide => ({
    insertText: {
      objectId: slide.title,
      text: slide.bullets
    },
  }));

  // Concatenate the requests array into a single array
  const batchUpdateRequest = {
    requests: [...requests],
  };

  // Make the batchUpdate request to the Slides API
  slides.presentations.batchUpdate(
  { presentationId, requestBody: batchUpdateRequest },
  (err, res) => {
    if (err) return console.error('The API returned an error:', err);
    console.log(`Page titles updated for presentation with ID: ${presentationId}`);
  });
}


module.exports = {
    main: main
};
