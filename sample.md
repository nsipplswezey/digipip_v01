# MICROLEARNING Series, Prisma Cloud Code Securtity: Feature in a Minute

### Learning Objectives
- Lead discussion on why the feature was built
- Demo the feature with 2 distinct use cases
- Wrap up the conversation with next steps

### Why this feature
- Industry tailwinds
- Customer value props
- Competitive advantages

### Next Steps
- Contact your CSM Paul Prowdgail
- Discuss licensing updates
- Build video microlearning
