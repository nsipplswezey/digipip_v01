// Successfully runs and update the TITLE of a TITLE_AND_BODY template
// As of 17FEB2023


// Specify the text you want to set as the new title
const newTitleText = 'Learning Objectives';

async function updatePageElementText(slideId, objectId, text) {
  const { google } = require('googleapis');
  const {GoogleAuth} = require('google-auth-library');

  const myVariable = process.env.PRESO_VAR;

  if (myVariable === 'true') {
    console.log(`$PRESO_VAR`)
  } else {
    console.log('No preso id')
  }


  const auth = new GoogleAuth({
    scopes:['https://www.googleapis.com/auth/presentations',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/presentations.readonly'],
    keyFilename: 'client_key.json'
  });

  const slides = google.slides({ version: 'v1', auth });
  const presentationId = myVariable;

  try {
    const { data: { replies } } = await slides.presentations.batchUpdate({
      presentationId,
      requestBody: {
        requests: [
          {
            insertText: {
              objectId: objectId,
              text: text
            },
            insertText:{
              objectId: 'Header3-body',
              text: 'newTitleText'
            }
          }
        ]
      }
    });

    console.log(`Updated ${replies.length} page elements:`);
    replies.forEach((reply) => console.log(reply));
  } catch (err) {
    console.error(`Error updating page element ${objectId} on slide ${slideId}: ${err}`);
    throw err;
  }
}

// Example usage

updatePageElementText(slideId='Header3', objectId='Header3-title', text='Learning Objectives')
  .catch((err) => console.error(err));
