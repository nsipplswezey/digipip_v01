// Successfully runs and creates fresh preso
// As of 17FEB2023

const { google } = require('googleapis');
const {GoogleAuth} = require('google-auth-library');

// Set up the service account credentials
async function main(folder, presoTitle) {
  const auth = new GoogleAuth({
    scopes:['https://www.googleapis.com/auth/presentations',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/presentations.readonly'],
    keyFilename: 'client_key.json'
  });

  // Set up the Google Drive and Slides APIs clients
  const drive = google.drive({ version: 'v3' , auth });
  const slides = google.slides({ version: 'v1', auth });

  // Set up the parameters for creating the presentation

  //const presentationId = process.env.PRESENTATION_ID
  const presentationTitle = presoTitle;
  const folderId = folder;
  const slideData = [
    {
      title: 'Header 1',
      bullets: ['Bullet point 1', 'Bullet point 2', 'Bullet point 3']
    },
    {
      title: 'Header 2',
      bullets: ['Bullet point A', 'Bullet point B', 'Bullet point C']
    },
    {
      title: 'Header 3',
      bullets: ['Bullet point X', 'Bullet point Y', 'Bullet point Z']
    }
  ];

  // Create a new presentation
  slides.presentations.create({
    requestBody: {
      title: presentationTitle
    }
  }).then(res => {
    const presentationId = res.data.presentationId;
    process.env.PRESO_VAR = presentationId;
    
    // Create slides with the titles
    const requests = slideData.map(data => {
      const bulletPoints = data.bullets.map(bullet => {
        return {
          text: {
            text: bullet
          }
        };
      });

      return {
        createSlide: {
          objectId: data.title.replace(/\s/g, ''),
          slideLayoutReference: {
            predefinedLayout: 'TITLE_AND_BODY',
          },
          insertionIndex: 0,
          placeholderIdMappings: [
            {
              layoutPlaceholder: {
                type: 'TITLE'
              },
              objectId: data.title.replace(/\s/g, '') + '-title'
            },
            {
              layoutPlaceholder: {
                type: 'BODY'
              },
              objectId: data.title.replace(/\s/g, '') + '-body'
            }
          ]
        }
      };
    });
    slides.presentations.batchUpdate({
      presentationId,

      requestBody: {
        requests
      }
    }).then(res => {
      console.log(`Created ${slideData.length} slides with titles.`);


      drive.files.update({
        fileId: presentationId,
        addParents: folderId
      }).then(res => {
        console.log(`Presentation added to folder with ID: ${folderId} and presentation ID: ${presentationId}`);
      }).catch(err => {
        console.error(`Error adding presentation to folder: ${err}`);
      });
    }).catch(err => {
      console.error(`Error creating slides: ${err}`);
    });
  }).catch(err => {
    console.error(`Error creating presentation: ${err}`);
  });
  
}



const folder = '1BplNrIuoL374eW_HhbXtgxnvkSrDY8dW'
main(folder, '[DRAFT] MICROLEARNING SERIES: SCA for License Compliance')
  .catch((err) => console.error(err));
